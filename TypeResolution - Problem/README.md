# Type Resolution - Problem

## Exercise
The goal of this exercise is to deduce what will be the result of the different functions called in `main()`

Of course you shall not run the program and look at the output before trying this exercise.

## Explaination of results

Read this section only if you have finished the exercise and whant some explaination on the result.

### `by_value`

Kind of the default c++ behaviour.

When passing an object by value to a function, a copy of this object if constructed on the stacked and passed as argument of the function.

In our case `by_value` expects a object of type `A` so the copy on the stack is of type `A`.

### `by_const_ref`

Did you expect something else? I don't blame you.

Virtualisation is resolved via a hidden `this` parameter. It means that a call to `f(a)` is in fact a call to `F::operator()(this, a)`.
The `this` parameter will be used to find the proper virtual call to use in a vtable.

This means that only a caller can by dynamically dispatched, parameters are statically dispatched at compile time.


### Exercise: Find a way to resolve both object in our expected type

In the solution we will see how to get the expected behaviour.

### `by_rvalue_ref`

In this case rvalue ref and const ref behave the same way, we're not using specific feature of rvalue ref.

### `templated`

Template argument resolution are resolved at compile time. All our cases are covered.