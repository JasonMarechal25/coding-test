# Type Resolution - Solution

## Fixing `by_const_ref`

Previously we saw that only the first argument was properly resolved in our expected type.

As explained, virtualisation is resolved thanks to the hidden `this` argument of a function. Since we want to resolve both virtualisation a way to get our result would be to have two call to functions with a `this` parameter.

`F` is already resolved with the function `operator`, we need to resolve `A` with a function, so let's add one in `A` and let's call it `process`.
What parameters are expected of `process` and what does it do? Let's take a step back from the problem at hand and let's see what we want *in fine*.

We want to call `F` on a object of the `A` hierarchy. This means the final call is `F(a)` and `a` must have been resolved properly. Since a object knows itself, the best way would be for `a` (`A` or `B`) to call `F()` on itself, `F` becoming a parameter of `process`.

We now have `void process(const F& f) { f(*this); }` or for `B`: `B::process(const B* this, const F& f) { f(*this); }`. The `A` object is properly resolved thanks to the `this` parameter, afterwards we fall back to what we saw before for `F`.
 `F` is resolved with its hidden `this` parameter, execpt this time the type of the argument passed to the function is know thanks to the previous resolution.

Last thing: we need to call `process` in `by_const_ref` for everything to work as intended.

## Some explainations

What we just did is called **double dispatch**. We dispatched `A` thanks to `process` and `F` thanks to `operator()`.

Some people may know this pattern as **Visitor**, just replace `process` by `accept`.