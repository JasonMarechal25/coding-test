#include <iostream>

class F;

class A {
public:
	virtual void process(const F& f) const;
};
class B : public A {
	virtual void process(const F& f) const override;
};

class F {
public:
	virtual inline void operator()(const A&) const noexcept {
		std::cout << "FxA" << std::endl;
	}
	virtual inline void operator()(const B&) const noexcept {
		std::cout << "FxB" << std::endl;
	}
};

class G : public F {
public:
	virtual inline void operator()(const A&) const noexcept {
		std::cout << "GxA" << std::endl;
	}
	virtual inline void operator()(const B&) const noexcept {
		std::cout << "GxB" << std::endl;
	}
};

void A::process(const F& f) const {
	f(*this);
}

void B::process(const F& f) const {
	f(*this);
}

void by_value(F f, A a) {
	f(a);
}

void by_const_ref(const F& f, const A& a) {
	a.process(f);
}

void by_rvalue_ref(F&& f, A&& a) {
	f(a);
}

template<typename f_type, typename a_type>
void templated(f_type&& f, a_type&& a) {
	std::forward<f_type>(f)(std::forward<a_type>(a));
}

int main() {
	std::cout << "By value\n";
	by_value(F(), A());
	by_value(F(), B());
	by_value(G(), A());
	by_value(G(), B());
	std::cout << "By const ref\n";
	by_const_ref(F(), A());
	by_const_ref(F(), B());
	by_const_ref(G(), A());
	by_const_ref(G(), B());
	std::cout << "By rvalue ref\n";
	by_rvalue_ref(F(), A());
	by_rvalue_ref(F(), B());
	by_rvalue_ref(G(), A());
	by_rvalue_ref(G(), B());
	std::cout << "Templated\n";
	templated(F(), A());
	templated(F(), B());
	templated(G(), A());
	templated(G(), B());


	return 0;
}
